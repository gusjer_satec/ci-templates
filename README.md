# GitLab CI Templates

Templates for GitLab CI (currently only for docker)

## Docker [Buildkit](https://github.com/moby/buildkit)
Builds a Dockerfile & pushes to GitLab's Container registry (tagged with commit sha & `latest`)

- using [moby/buildkit](https://github.com/moby/buildkit#readme) (best performance & caching according to my tests)
- full layer-based cache using a separate cache image pushed to Gitlab registry of your project
- easily customizable (used it for various projects for years now)
- auto-upgrading dependencies [via renovate](https://gitlab.com/txlab/ci-templates/-/merge_requests/8)
- stability via [releases](https://gitlab.com/txlab/ci-templates/-/releases)

```yml
include:
  - project: txlab/ci-templates # https://gitlab.com/txlab/ci-templates/
    file: docker-buildkit.gitlab-ci.yml
    # ref: vX.Y - use a tag as fixed version (pro tip: renovate bot will detect upgrades)

docker_build:
  extends: .docker_buildkit # or to use GitLab dependency proxy: .docker_buildkit_dependencyproxy
  # < CUSTOMIZATIONS HERE > #
```
(on a self-hosted instance, you need to include via HTTP:)
```yaml
include:
  - remote: https://gitlab.com/txlab/ci-templates/-/raw/main/docker-buildkit.gitlab-ci.yml
    # Pro tip: Use url with tag name (e.g. from https://gitlab.com/txlab/ci-templates/-/releases) - then renovate bot will upgrade
```

### Customize Docker context
```yml
variables:
  DOCKER_CONTEXT: everything-docker-needs/ # (default: project root)
  DOCKERFILE_DIR: everything-docker-needs/docker-files/ # NOT relative to the above (default: $DOCKER_CONTEXT, so optional if it's the same)
  DOCKERFILE_PATH: Custom.dockerfile # relative to the above (default: 'Dockerfile')
```

### Customize push
Default rules are to always push with commit SHA & as `latest` tag only on default branch.

Override `rules` if you want something different, e.g.:
```yml
variables:
  PUSH_IMAGE_BASE: '$CI_REGISTRY_IMAGE/testimage'
rules:
  - if: '$CI_COMMIT_BRANCH == "dev"'
    variables:
      PUSH_IMAGES: 'name=$PUSH_IMAGE_BASE:$CI_COMMIT_SHA,$PUSH_IMAGE_BASE:dev'
  - !reference [.docker_buildkit, rules] # if you want the default rules
```

#### Docker Hub
Secrets:
- `DOCKER_HUB_USER`: `foosername`
- `DOCKER_HUB_PASSWORD`: `prefer-access-token`
```yaml
variables:
  PUSH_IMAGES_MAIN: 'name=$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA,$CI_REGISTRY_IMAGE:latest,docker.io/foosername/hello-world:latest'
  # or only to docker hub: name=docker.io/foosername/hello-world:latest
```

#### Custom registry
```yaml
variables:
  PUSH_IMAGES: name=registry.my.org/foosername/hello-world:$CI_COMMIT_SHA
before_script:
  - !reference [.docker_buildkit, before_script]
  # My Registry proxy login
  - >
    cat ~/.docker/config.json | 
    jq ".auths += {\"registry.my.org\": {
      auth: \"$(echo -n "$MY_REGISTRY_USER:$MY_REGISTRY_PASSWORD" | base64)\"
    }}" | sponge ~/.docker/config.json
```

### Customize script before build
```yaml
script:
  # Add version to commit.txt
  - echo "$CI_COMMIT_SHA" > commit.txt
  - !reference [.docker_buildkit, script]
```

### Customize cache image
Default is one cache image for the whole repo, this would change to one per job (warning: no spaces or special chars allowed):
```yml
variables:
  DOCKER_CACHE_IMAGE: $CI_REGISTRY_IMAGE:cache-$CI_JOB_NAME
```

### Override cache arguments (e.g. no cache or local cache)
```yml
variables:
  BUILDKIT_CACHE_ARGS: '--export-cache type=local,mode=max,...'
```

### Customize target platform/architecture
See [buildkit docs](https://github.com/moby/buildkit/blob/master/docs/multi-platform.md)
```yml
variables:
  TARGETPLATFORM: linux/arm/v7,linux/amd64
```

### Additional buildkit arguments (e.g. build args)
```yml
variables:
  BUILDKIT_ARGS: '--opt build-arg:foo=bar'
```

## Example usages

### 1. [Simple build & push to docker hub](https://gitlab.com/TeNNoX/docker-alpine-basics/-/blob/master/.gitlab-ci.yml?ref_type=heads)
### 2. [Scheduled cache disabling](https://gitlab.com/txlab/docker/nginx-static/-/blob/main/.gitlab-ci.yml?ref_type=heads)
